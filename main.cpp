#include <SFML/Graphics.hpp>
#include "include/SimpleLogger.h"
#include "include/NavigationNode.h"
#include "include/MovingObject.h"
#include "include/Room.h"
#include "include/Door.h"

int main()
{
    std::string const APP_NAME  =   "Test SFML LERP";
    sf::Clock clock;

    sf::RenderWindow window(sf::VideoMode(800, 600), APP_NAME, sf::Style::Default);

    SimpleLogger appLogger(LOG_LEVEL::ALL, "debug.log");
    appLogger.setWriteToFile(true);
    appLogger.LOG(LOG_LEVEL::ALL, "Starting application : " + APP_NAME);


    appLogger.LOG(LOG_LEVEL::DEBUG, "Création des Nodes de nav...");
    NavigationNode navPoint(sf::Vector2f(40, 400));
    Door enterDoor(sf::Vector2f(300, 360));
    Door exitDoor(sf::Vector2f(500, 260));

    appLogger.LOG(LOG_LEVEL::DEBUG, "Création des objets...");
    MovingObject lil_guy(sf::Vector2f(25, 50));
    lil_guy.setPosition(sf::Vector2f(400, 40));
    lil_guy.addNavPoint(navPoint);
    lil_guy.addNavPoint(enterDoor);
    lil_guy.addNavPoint(exitDoor);
    lil_guy.computeDirection();

    Room mainRoom(Vector2f( 200, 300));
    mainRoom.setPosition(400, 300);

    while (window.isOpen())
    {
        sf::Time deltaTime  =   clock.restart();
        sf::Event event;
        while (window.pollEvent(event))
        {

            if (event.type == sf::Event::Closed)
            {
                window.close();
            }

        }
        //UPDATE HERE
        lil_guy.updatePosition(deltaTime);


        window.clear();
        window.draw(navPoint);
        window.draw(mainRoom);
        window.draw(enterDoor);
        window.draw(exitDoor);
        window.draw(lil_guy);
        window.display();
    }

    return 0;
}
