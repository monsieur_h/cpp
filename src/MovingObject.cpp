#include "MovingObject.h"
#include <cmath>

#include <iostream>

using namespace sf;

MovingObject::MovingObject(Vector2f pSize)
{
    setSize(pSize);
    setOrigin(Vector2f(pSize.x/2, pSize.y/2));
    setFillColor(Color(255, 100, 0, 150));
    speed = 256;
    direction  = Vector2f(0, 0);
}

void MovingObject::computeDirection()
{
    if(pathNodeQueue.empty())
    {

    }
    else
    {
        direction = pathNodeQueue.front().getPosition() - getPosition();
        double len= sqrtf( (direction.x * direction.x) + (direction.y * direction.y) );
        direction.x = direction.x / len;
        direction.y = direction.y / len;
    }
}

void MovingObject::updatePosition(Time pDt)
{
    if(direction.x != 0 && direction.y != 0)
    {
        Vector2f movement(direction);
        movement.x *= pDt.asSeconds() * speed;
        movement.y *= pDt.asSeconds() * speed;
        move(movement);
        if(isDestinationReached())
        {
            onDestinationReached();
        }
    }
}

void MovingObject::stop()
{
    direction = Vector2f(0, 0);
}

void MovingObject::onDestinationReached()
{
    Vector2f targetPos = pathNodeQueue.front().getPosition();
    setPosition(targetPos);
    direction  = Vector2f(0, 0);

    pathNodeQueue.pop();
    std::cout << "Destination reached, next waypoint?" << std::endl;
    if(pathNodeQueue.empty())
    {
        stop();
    }
    else
    {
        computeDirection();
    }
}

bool MovingObject::isDestinationReached()
{
    Vector2f v_distance;
    v_distance = pathNodeQueue.front().getPosition() - getPosition();
    return ( (v_distance.x*direction.x) < 0 ); //negative if they are not of the same sign, hence if we reached/passed the NavigationNode
}

void MovingObject::addNavPoint(NavigationNode pNavPoint)
{
    pathNodeQueue.push(pNavPoint);
}

MovingObject::~MovingObject()
{
    //dtor
}
