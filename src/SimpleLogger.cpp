#include "SimpleLogger.h"

//Constructors
SimpleLogger::SimpleLogger(LOG_LEVEL::LOG_LEVEL pLogLevel) : logLevel(pLogLevel), filename("lastsession.log"), writeToFile(true), fileOpened(false)
{

}

SimpleLogger::SimpleLogger(LOG_LEVEL::LOG_LEVEL pLogLevel, std::string pFilename) : logLevel(pLogLevel), filename(pFilename), writeToFile(true), fileOpened(false)
{

}


//Members
void SimpleLogger::LOG(LOG_LEVEL::LOG_LEVEL pLogLevel, std::string pLogString)
{
    if(pLogLevel >= logLevel)
    {
        std::cout << pLogString << std::endl;
        if(writeToFile)
        {
            write(pLogString);
        }
    }
}

void SimpleLogger::setWriteToFile(bool pWrite)
{
    writeToFile = pWrite;
}

void SimpleLogger::write(std::string pLog)
{
    if(!fileOpened)
    {
        openFile();
    }
    currentLogFile << pLog << std::endl;
}

void SimpleLogger::openFile()
{
    currentLogFile.open(filename.c_str());
    fileOpened = true;
}

void SimpleLogger::closeFile()
{
    currentLogFile.close();
    fileOpened = false;
}

void SimpleLogger::setFilename(std::string pName)
{
    closeFile();
    filename = pName;
    openFile();
}

SimpleLogger::~SimpleLogger()
{

}
