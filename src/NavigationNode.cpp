#include "NavigationNode.h"
#include <iostream>

using namespace sf;

NavigationNode::NavigationNode(Vector2f pPos)
{
    setRadius(5.f);
    setOrigin( getRadius(), getRadius() );
    setPosition(pPos);
    setFillColor(Color(200, 200, 100, 100));
}
