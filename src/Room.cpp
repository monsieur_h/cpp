#include "Room.h"
#include <SFML/Graphics.hpp>

using namespace sf;

Room::Room(Vector2f pSize)
{
    setSize(pSize);
    setOrigin(Vector2f(pSize.x/2, pSize.y/2));
    setFillColor(Color(100, 0, 255, 150));
    setOutlineColor(Color(255, 255, 255, 230));
    setOutlineThickness(5.0f);
}

