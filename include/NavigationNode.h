#ifndef NAVIGATIONNODE_H
#define NAVIGATIONNODE_H

#include <SFML/Graphics.hpp>

//Represents a node that can be targeted by a moving element. (Such as a door, a working position or so)
class NavigationNode : public sf::CircleShape
{
    public:
        NavigationNode(sf::Vector2f pPos);

    private:
};

#endif // NAVIGATIONNODE_H
