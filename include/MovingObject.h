#ifndef MOVINGOBJECT_H
#define MOVINGOBJECT_H

#include <SFML/Graphics.hpp>
#include "NavigationNode.h"
#include <queue>

using namespace sf;

class MovingObject : public RectangleShape
{
    public:
        MovingObject(Vector2f pSize);
        void updatePosition(Time dt);
        void addNavPoint(NavigationNode nextNavpoint);//Adds a new NavigationNode to the queue
        void stop();
        void computeDirection();


        virtual ~MovingObject();

    private:
        float speed;
        Vector2f direction;
        NavigationNode *finalDestination; //Pointer to final destination of the pathfinding.

        std::queue<NavigationNode> pathNodeQueue;

        bool isDestinationReached();
        void onDestinationReached();
};

#endif // MOVINGOBJECT_H
