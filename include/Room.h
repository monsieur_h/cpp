#ifndef ROOM_H
#define ROOM_H

#include <SFML/Graphics.hpp>
#include <vector>
#include "NavigationNode.h"
#include "Door.h"

using namespace sf;

class Room : public RectangleShape
{
    public:
        Room(Vector2f pSize);
        void appendDoor(Door pDoor);
//        void appendSpot(Spot pSpot);
        void draw(RenderWindow &pWindow);

    private:
        std::vector <NavigationNode> spotList;
        std::vector <Door> doorList;
};

#endif // ROOM_H
