#ifndef SIMPLELOGGER_H
#define SIMPLELOGGER_H

#include <string>
#include <iostream>
#include <fstream>

namespace LOG_LEVEL
{
    enum LOG_LEVEL
    {
        NONE,
        RELEASE,
        DEBUG,
        ALL
    };
};


class SimpleLogger
{
    public:
        SimpleLogger(LOG_LEVEL::LOG_LEVEL);
        SimpleLogger(LOG_LEVEL::LOG_LEVEL, std::string);
        void LOG(LOG_LEVEL::LOG_LEVEL, std::string);
        void setWriteToFile(bool);
        void setFilename(std::string);

        //TODO:Theses
        void openFile();
        void closeFile();
        void write(std::string);

        virtual ~SimpleLogger();

    protected:

    private:
        std::string filename;
        int logLevel;
        bool writeToFile;
        bool fileOpened;
        std::ofstream currentLogFile;
};

#endif // SIMPLELOGGER_H
