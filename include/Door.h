#ifndef DOOR_H
#define DOOR_H

#include "NavigationNode.h"
#include <SFML/Graphics.hpp>

using namespace sf;

class Door : public NavigationNode
{
    public:
        Door(Vector2f pPos);

    private:
};

#endif // DOOR_H
